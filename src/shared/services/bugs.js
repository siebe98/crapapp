import { apiUrls } from "../../config";

export function getBugsFromDB() {
    return fetch(apiUrls.bugs)
}

export function updateBugs(values, bugs) {
   bugs.forEach(bug => {
       bug.active = values[bug.code]
   });

    return fetch(apiUrls.bugs, {
        method: 'POST',
        headers: {
            "Content-Type" : "application/json"
          },
          body: JSON.stringify(bugs)
    });
}

export function getAllBugs() {
    createBugsInSession();

    return JSON.parse(window.sessionStorage.getItem('bugs'));
}

function createBugsInSession() {
    getBugsFromDB().then((r) => {
        if (r.status === 200) {
            r.text().then((bugs) => {
                window.sessionStorage.setItem('bugs', bugs);
            });
        }
    });
}