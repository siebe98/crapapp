import React, { Component, Fragment } from 'react';

import { routes, regex } from '../../config';
import GenderSelectionField from '../../shared/components/genderSelectionField';
import { FormBuilder, Validators, FieldGroup, FieldControl } from 'react-reactive-form';
import TextInput from '../../shared/components/textInput';
import PasswordInput from '../../shared/components/passwordInput';
import { Redirect } from 'react-router-dom';
import { checkIfMatchingPassword } from '../../shared/services/password';
import { registerUser } from '../../shared/services/register';
import Banner from '../../shared/components/banner';

class RegisterScreenView extends Component {
    registerForm = FormBuilder.group({
        email: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.email)
        ])],
        username: ['', Validators.required],
        password: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.password)
        ])],
        confirmPassword: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.password)
        ])],
        gender: ['', Validators.required]
    },
        { validators: checkIfMatchingPassword('password', 'confirmPassword') }
    );


    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        }
    }

    render() {
        return (
            this.state.redirect ? <Redirect to={routes.confirmationView} /> :
                <Fragment>
                    <FieldGroup
                        control={this.registerForm}
                        render={({ invalid }) => (
                            <form>
                                <Banner />
                                <div className="row">
                                    <div className="col s5 offset-s1">
                                        <div className="card">
                                            <div className="card-content">
                                                <FieldControl
                                                    name="email"
                                                    render={TextInput}
                                                    meta={{ label: "email" }}
                                                />

                                                <FieldControl
                                                    name="username"
                                                    render={TextInput}
                                                    meta={{ label: "Username" }}
                                                />

                                                <FieldControl
                                                    name="gender"
                                                    render={GenderSelectionField}
                                                    meta={{ label: "Gender" }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s4 offset-s1">
                                        <div className="card">
                                            <div className="card-content">
                                                <FieldControl
                                                    name="password"
                                                    render={PasswordInput}
                                                    meta={{ label: "Password" }}
                                                />
                                                <FieldControl
                                                    name="confirmPassword"
                                                    render={PasswordInput}
                                                    meta={{ label: "Confirm password" }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s5 offset-s1">
                                        <div className="row">
                                            <a className="waves-effect waves-light btn" disabled={invalid} onClick={() => this.register()} id="RegisterButton">Register</a>
                                        </div>
                                        <div className="row">
                                            <a href={routes.login} className="waves-effect waves-light btn secondary" id="CancelButton">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        )}
                    />
                </Fragment>
        );
    }

    register() {
        registerUser(this.registerForm.value).then((response) => {
            if (response.status === 201) {
                this.setState({ redirect: true });
            } else if (response.status === 409) {
                alert("The username you chose has already been taken");
            } else if (response.status === 412) {
                alert("The emailaddress is already in use");
            }
        })
    }
}

export default RegisterScreenView;